package br.com.lp3.trabalho.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.trabalho.dto.ClienteDTO;
import br.com.lp3.trabalho.model.Cliente;
import br.com.lp3.trabalho.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public ClienteDTO findById(Long id) {
		Optional<Cliente> oCliente = clienteRepository.findById(id);
		if (oCliente != null && oCliente.isPresent()) {
			Cliente cliente = oCliente.get();
			ClienteDTO clienteDTO = new ClienteDTO(cliente.getId(), cliente.getNome(), cliente.getCpf());
			return clienteDTO;
		} else {
			return null;
		}
		
	}
	
	public ClienteDTO saveCliente(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
		cliente = clienteRepository.save(cliente);
		clienteDTO.setId(cliente.getId());
		return clienteDTO;
	}
	
	public ClienteDTO delete(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
	    clienteRepository.delete(cliente);
	    return clienteDTO;
	   }
	
	public ClienteDTO update(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
	    cliente = clienteRepository.update(cliente);
	    clienteDTO.setId(cliente.getId());
	    return clienteDTO;
	   }
	
}
