package br.com.lp3.trabalho.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.lp3.trabalho.dto.PedidoDTO;
import br.com.lp3.trabalho.model.Pedido;
import br.com.lp3.trabalho.repository.PedidoRepository;

public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	public PedidoDTO findById(Integer id) {
		Optional<Pedido> oPedido = pedidoRepository.findById(id);
		if (oPedido != null && oPedido.isPresent()) {
			Pedido pedido = oPedido.get();
			PedidoDTO pedidoDTO = new PedidoDTO(pedido.getId(), pedido.getDescricao(), pedido.getProduto(), pedido.getCliente(), pedido.getQtde(), pedido.getValorTotal());
			return pedidoDTO;
		} else {
			return null;
		}
		
	}
	
	public PedidoDTO savePedido(PedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido(pedidoDTO.getId(), pedidoDTO.getDescricao(), pedidoDTO.getProduto(), pedidoDTO.getCliente(), pedidoDTO.getQtde(), pedidoDTO.getValorTotal());
		pedido = pedidoRepository.save(pedido);
		pedidoDTO.setId(pedido.getId());
		return pedidoDTO;
	}
	
	public PedidoDTO delete(PedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido(pedidoDTO.getId(), pedidoDTO.getDescricao(), pedidoDTO.getProduto(), pedidoDTO.getCliente(), pedidoDTO.getQtde(), pedidoDTO.getValorTotal());
	    pedidoRepository.delete(pedido);
	    return pedidoDTO;
	   }
	
	public PedidoDTO update(PedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido(pedidoDTO.getId(), pedidoDTO.getDescricao(), pedidoDTO.getProduto(), pedidoDTO.getCliente(), pedidoDTO.getQtde(), pedidoDTO.getValorTotal());
	    pedido = pedidoRepository.update(pedido);
	    pedidoDTO.setId(pedido.getId());
	    return pedidoDTO;
	   }

}
