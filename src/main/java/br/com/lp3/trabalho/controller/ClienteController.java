package br.com.lp3.trabalho.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.trabalho.dto.ClienteDTO;
import br.com.lp3.trabalho.service.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ClienteDTO> findClienteById(@PathVariable Long id) {
		ClienteDTO clienteDTO = clienteService.findById(id);
		if (clienteDTO != null) {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<ClienteDTO> saveCliente(@RequestBody ClienteDTO clienteDTO) {
		clienteDTO = clienteService.saveCliente(clienteDTO);
		if (clienteDTO != null) {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/cliente/{id}")
	public ResponseEntity<ClienteDTO> deleteCliente(@RequestBody ClienteDTO clienteDTO) {
		clienteDTO = clienteService.delete(clienteDTO);
			if (clienteDTO != null) {
				return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
	   }
	
	@PutMapping("/cliente/{id}")
	public ResponseEntity<ClienteDTO> updateCliente(@RequestBody ClienteDTO clienteDTO) {
		clienteDTO = clienteService.update(clienteDTO);
			if (clienteDTO != null) {
				return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
	   }
	
	
}
