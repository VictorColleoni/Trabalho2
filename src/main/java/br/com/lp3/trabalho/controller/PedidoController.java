package br.com.lp3.trabalho.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.lp3.trabalho.dto.PedidoDTO;
import br.com.lp3.trabalho.service.PedidoService;

public class PedidoController {
	
	@Autowired
	private PedidoService pedidoService;

	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<PedidoDTO> findProdutoById(@PathVariable Integer id) {
		PedidoDTO pedidoDTO = pedidoService.findById(id);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<PedidoDTO> savePedido(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.savePedido(pedidoDTO);
		if (pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/pedido/{id}")
	public ResponseEntity<PedidoDTO> deleteProduto(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.delete(pedidoDTO);
			if (pedidoDTO != null) {
				return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
	   }
	
	@PutMapping("/pedido/{id}")
	public ResponseEntity<PedidoDTO> updateProduto(@RequestBody PedidoDTO pedidoDTO) {
		pedidoDTO = pedidoService.update(pedidoDTO);
			if (pedidoDTO != null) {
				return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
	   }
}
