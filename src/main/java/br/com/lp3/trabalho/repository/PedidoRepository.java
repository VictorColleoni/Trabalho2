package br.com.lp3.trabalho.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lp3.trabalho.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer> {

	Pedido update(Pedido pedido);

}
