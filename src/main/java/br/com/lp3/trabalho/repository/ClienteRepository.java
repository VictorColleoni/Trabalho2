package br.com.lp3.trabalho.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.trabalho.model.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{

	Cliente update(Cliente cliente);

}
